package main

import "os"

// Help is displayed when after an erroneous command-line option is parsed, or
// the user specifies the help option flag. This function should override the
// usage function in the flag package for the entire application. 
func help() {
    println(`
NAME
    Portals - Conquer Online Portal Editor
    Copyright 2018 Gareth Jensen, "Spirited"
    Build 2018.11.15

ABSTRACT
    portals [-o outputfile] [-v] dmapfile

DESCRIPTION
    Portals is a program for modifying Conquer Online data maps (DMaps) with 
    custom portal placement. Replaces the portal section header and content 
    with portal information passed in from standard input.

ARGUMENTS
    dmapfile   File path to the data map to be read or modified

OPTIONS
    -o / -output     Output filename if not replacing the data map
    -v / -verbose    Enables verbose logging (false)
    -? / -help       Shows help documentation and exits the program (false)

EXAMPLES
    portals desert.dmap > portals.txt
    portals desert.dmap < portals.txt
    cat portals.txt | portals desert.dmap
    type portals.txt | portals -o desert_modified.dmap desert.dmap`)
    os.Exit(0)
}