package main

import (
    "os"
)

// Portals is a program for modifying Conquer Online data maps (DMaps) with 
// custom portal placement. Replaces the portal section header and content with
// portal information passed in from standard input.
func main() {
    stat, _ := os.Stdin.Stat()
    if (stat.Mode() & os.ModeCharDevice) == 0 {
        writePortals()
    } else {
        readPortals()
    }
}
