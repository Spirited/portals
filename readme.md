# Portals Editor
A simple command-line tool for editing portal locations in a data map (DMap) file. The tool outputs a new-line separated list of portal records for editing in the format of index,x,y. It accepts the same list and formatting when modifying portal records for a DMap. The tool can be compiled using the [golang compiler](https://golang.org/dl/) for Windows, Mac, or Linux. 

## Build Instructions
```
go get spirited.io/portals
go install spirited.io/portals
```

## Readme
```
NAME
    Portals - Conquer Online Portal Editor
    Copyright 2018 Gareth Jensen, "Spirited"
    Build 2018.11.15

ABSTRACT
    portals [-o outputfile] [-v] dmapfile

DESCRIPTION
    Portals is a program for modifying Conquer Online data maps (DMaps) with 
    custom portal placement. Replaces the portal section header and content 
    with portal information passed in from standard input.

ARGUMENTS
    dmapfile   File path to the data map to be read or modified

OPTIONS
    -o / -output     Output filename if not replacing the data map
    -v / -verbose    Enables verbose logging (false)
    -? / -help       Shows help documentation and exits the program (false)

EXAMPLES
    portals desert.dmap > portals.txt
    portals desert.dmap < portals.txt
    cat portals.txt | portals desert.dmap
    type portals.txt | portals -o desert_modified.dmap desert.dmap
```
